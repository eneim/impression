# Impression
A material design gallery app for Android 4.1+

Developed by Aidan Follestad, Daniel Ciao, and Marlon Jones.

Project in progress of revival. "Watch" the repo for updates.

##License
```
Impression: A material design gallery app for Android 4.1+
Copyright (C) 2015  Aidan Follestad, Daniel Ciao, and Marlon Jones

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
```
[Quick rundown of GPLv3](http://choosealicense.com/licenses/gpl-3.0/) - our intention is to encourage open source and sharing of derivative works.
